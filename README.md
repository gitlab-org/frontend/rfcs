# RFCs for changes to GitLab Frontend

Would you like to propose a new or a change to an existing frontend pattern in our codebase? Please create an issue here so that we can discuss it using the RFC (Request for Comments) process.

## Why should we use this process?

We used to discuss change proposals in gitlab-ce issues but we have now scaled frontend to such a size in which it is no longer effective to discuss them in the gitlab-ce project. Our previous method was
- Difficult to surface historical conversations around specific frontend process decisions
- Difficult for the team to track what changes are being proposed and implemented

## Goals of this process
- Single Source of Truth (SSOT) to track all frontend process changes, so that it's easy for new team members and the wider GitLab community to understand certain decisions
- Create a straightforward format for the team to make, discuss and implement decisions regarding frontend process changes
- Ensure that there is a DRI (Directly Responsible Individual) so that proposals don't go stale

## How will this RFC process work?

See the progress of our RFCs in this [issue board](https://gitlab.com/gitlab-org/frontend/rfcs/-/boards/1184859)

1. Create an issue using the templates provided, including a reference implementation. (See below for more about reference implementations.)
    -  If an issue is not using a template it can be automatically rejected.
1. Share the RFC issue on the frontend weekly call and on the #frontend slack channel
1. Give the frontend team some time to review the proposal and provide comments about the proposal
1. If there is a strong consensus towards a decision, and it is a [two-way door decision](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions),
    - label the issue as `will implement` and select a frontend team member to lead the implementation of the process and share this with the team
    - after the implementation is complete, change the label from `will implement` to `implemented` and close the issue
1. Otherwise, assign the `~needs-EM-decision` issue label. The Frontend Engineering Managers will review to make the final decision based on what is presented in the issue and comment on it with the decision and reasoning
    - If the decision is to proceed, we should follow the same steps as step 4.
    - If the decision is not to proceed, the issue should be closed. If there is a disagreement, we should still [disagree, commit and disagree](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree)
    - Before the implementation or closing the issue, the Frontend EMs will give Frontend maintainers a week to raise concerns.

### What are reference implementations and why do we use them?
When an RFC introduces a new pattern, like [how to style components](https://gitlab.com/gitlab-org/frontend/rfcs/issues/2) or a [messaging system between Vue and jQuery components](https://gitlab.com/gitlab-org/frontend/rfcs/issues/19), the RFC describes high-level design decisions about how developers should implement the pattern in the codebase. In practice, implementing these patterns can uncover technical constraints and low-level design decisions that can profoundly change the high-level guidelines in the RFC.

In addition, slightly different understandings of the proposed changes can be cleared up when the team looks at a concrete example.

A reference implementation should satisfy the following conditions:

- Address a real-world scenario. Create the reference implementation by applying the pattern proposed in the RFC in an existing feature of GitLab codebase.
- Provide metrics that demonstrate performance or productivity improvements.

As the RFC evolves, the reference implementation should evolve along with it. Ideally, when an RFC is accepted, the reference implementation should be able to be merged as the first step of implementing the RFC as a whole. 

In some cases, it may not make sense to create a reference implementation because the proposal is not about a change to code practices or patterns. In these situations, consider replacing the reference implementation with another dive into the details: a cost-benefit analysis, example documentation, etc.
